#ifndef SET_TAGS_MARK_POINT_DIALOG_HPP
#define SET_TAGS_MARK_POINT_DIALOG_HPP

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "qt.hpp"

#include <QDialog>

namespace Ui {
class SetTagsMarkPointDialog;
}

class SetTagsMarkPointDialog : public QDialog
{
    Q_OBJECT

public:
    SetTagsMarkPointDialog(QString &data, QWidget *parent = nullptr);
    virtual ~SetTagsMarkPointDialog();
    QString GetResult();

private slots:
    void accept();
    void reject();

private:
    Ui::SetTagsMarkPointDialog *ui;
};

#endif // SET_TAGS_MARK_POINT_DIALOG_HPP
