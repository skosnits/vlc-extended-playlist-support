#include "set_tags_mark_point_dialog.hpp"
#include "ui/set_tags_mark_point_dialog.h"

#include <vlc_intf_strings.h>
#include <QLabel>
#include <QLineEdit>

SetTagsMarkPointDialog::SetTagsMarkPointDialog(QString &data, QWidget *parent) :
    QDialog(parent, Qt::WindowTitleHint | Qt::WindowCloseButtonHint),
    ui(new Ui::SetTagsMarkPointDialog)
{
    ui->setupUi(this);
    this->setWindowTitle(qtr( I_MP_SET_TAGS ));
    ui->label->setText(qtr( I_MP_SET_TAGS_HELP ));
    ui->lineEdit->setText(data);
    ui->lineEdit->setFocus();
}

SetTagsMarkPointDialog::~SetTagsMarkPointDialog()
{
    delete ui;
}

QString SetTagsMarkPointDialog::GetResult()
{
    return ui->lineEdit->text();
}

void SetTagsMarkPointDialog::accept()
{
    QDialog::accept();
}

void SetTagsMarkPointDialog::reject()
{
    QDialog::reject();
}

