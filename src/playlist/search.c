/*****************************************************************************
 * search.c : Search functions
 *****************************************************************************
 * Copyright (C) 1999-2009 VLC authors and VideoLAN
 * $Id$
 *
 * Authors: Clément Stenac <zorglub@videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <assert.h>

#include <vlc_common.h>
#include <vlc_url.h>
#include <vlc_playlist.h>
#include <vlc_charset.h>
#include "playlist_internal.h"

#include <regex.h>

/***************************************************************************
 * Item search functions
 ***************************************************************************/

/***************************************************************************
 * Live search handling
 ***************************************************************************/

/**
 * Enable all items in the playlist
 * @param p_root: the current root item
 */
static void playlist_LiveSearchClean( playlist_item_t *p_root )
{
    for( int i = 0; i < p_root->i_children; i++ )
    {
        playlist_item_t *p_item = p_root->pp_children[i];
        if( p_item->i_children >= 0 )
            playlist_LiveSearchClean( p_item );
        p_item->i_flags &= ~PLAYLIST_DBL_FLAG;
    }
}

// <regex.h> POSIX regex does not support lookarounds.
// Even if use REG_EXTENDED you will get 13 error code REG_BADRPT.
// So we can add this support from C++ or use POSIX C Extended version by default.
bool (*regex_func)(const char *str, const char *search_data) = NULL;

void regex_SetFunction ( bool (*f)(const char *str, const char *search_data) )
{
    regex_func = f;
}

bool check_data(const char *str, const char *search_data, bool use_regex)
{
    bool res = false;

    if (str && search_data)
    {
        if (use_regex)
        {
            if (regex_func)
            {
                res = regex_func(str, search_data);
            }
            else
            {
                regex_t regex;
                int reti = regcomp(&regex, search_data, REG_EXTENDED | REG_ICASE);

                if (!reti)
                {
                    reti = regexec(&regex, str, 0, NULL, 0);

                    if (!reti)
                    {
                        res = true;
                    }

                    regfree(&regex);
                }
            }
        }
        else
        {
            res = vlc_strcasestr( str, search_data );
        }
    }

    return res;
}

/**
 * Enable/Disable items in the playlist according to the search argument
 * @param p_root: the current root item
 * @param psz_string: the string to search
 * @return true if an item match
 */
static bool playlist_LiveSearchUpdateInternal( playlist_item_t *p_root,
                                               const char *psz_string, bool b_recursive )
{
    int i;
    bool b_match = false;
    bool use_regex = (psz_string && (*psz_string == '>')) ? true : false;
    const char *search_data = use_regex ? psz_string + 1 : psz_string;

    for( i = 0 ; i < p_root->i_children ; i ++ )
    {
        bool b_enable = false;
        playlist_item_t *p_item = p_root->pp_children[i];
        // Go recurssively if their is some children
        if( b_recursive && p_item->i_children >= 0 &&
            playlist_LiveSearchUpdateInternal( p_item, psz_string, true ) )
        {
            b_enable = true;
        }

        if( !b_enable )
        {
            vlc_mutex_lock( &p_item->p_input->lock );

            if(strncmp(search_data, "tags:", strlen("tags:")) == 0)
            {
                char *input_item_tags, *search_tags, *str, *tag, *tmp;
                search_tags = search_data + strlen("tags:");
                int search_tags_len = strlen(search_tags);

                if (search_tags_len > 0)
                {
                    for (int i = 0 ; i < p_item->p_input->i_options; i++)
                    {
                        if(strncmp(p_item->p_input->ppsz_options[i], "mark_point=", strlen("mark_point=")) == 0)
                        {
                            if (input_item_tags = strstr(p_item->p_input->ppsz_options[i], ":"))
                            {
                                input_item_tags++;
                                if (input_item_tags = strstr(input_item_tags, ":"))
                                {
                                    input_item_tags++; // now it will contain mark point range tags
                                    int input_item_tags_len = strlen(input_item_tags);
                                    char *ext_input_item_tags = malloc(input_item_tags_len + 3);
                                    memcpy(ext_input_item_tags+1, input_item_tags, input_item_tags_len);
                                    ext_input_item_tags[0] = ',';
                                    ext_input_item_tags[input_item_tags_len+1] = ',';
                                    ext_input_item_tags[input_item_tags_len+2] = '\0';

                                    if (use_regex)
                                    {
                                        b_enable = check_data(ext_input_item_tags, search_tags, use_regex);
                                    }
                                    else
                                    {
                                        str = search_tags;
                                        do {
                                            tmp = strchr(str, ',');
                                            int search_tag_len = strlen(str) - (tmp ? strlen(tmp) : 0);

                                            if (search_tag_len > 0)
                                            {
                                                char *search_pattern = malloc(search_tag_len + 3);
                                                memcpy(search_pattern+1, str, search_tag_len);
                                                search_pattern[0] = ',';
                                                search_pattern[search_tag_len+1] = ',';
                                                search_pattern[search_tag_len+2] = '\0';

                                                if (strstr(ext_input_item_tags, search_pattern))
                                                {
                                                    b_enable = true;
                                                }
                                                else
                                                {
                                                    b_enable = false;
                                                    free(search_pattern);
                                                    break;
                                                }

                                                free(search_pattern);
                                            }

                                            str = tmp + 1;
                                        } while(tmp);
                                    }

                                    free(ext_input_item_tags);
                                }
                            }
                        }

                        if (b_enable)
                            break;
                    }
                }
            }
            else if(strncmp(search_data, "title:", strlen("title:")) == 0)
            {
                const char *psz_title = NULL;
                if( p_item->p_input->p_meta )
                {
                    // Use Title or fall back to psz_name
                    psz_title = vlc_meta_Get( p_item->p_input->p_meta, vlc_meta_Title );
                    if( !psz_title )
                        psz_title = p_item->p_input->psz_name;
                }
                else
                {
                    psz_title = p_item->p_input->psz_name;
                }

                b_enable = check_data( psz_title, search_data + strlen("title:"), use_regex);
            }
            else if(strncmp(search_data, "fp:", strlen("fp:")) == 0)
            {
                char* psz_path = input_item_GetFilePath(p_item->p_input);
                b_enable = check_data(psz_path, search_data + strlen("fp:"), use_regex);
                free( psz_path );
            }
            else if(strncmp(search_data, "album:", strlen("album:")) == 0)
            {
                if( p_item->p_input->p_meta )
                {
                    const char *psz_album = vlc_meta_Get( p_item->p_input->p_meta, vlc_meta_Album );
                    b_enable = check_data(psz_album, search_data + strlen("album:"), use_regex);
                }
            }
            else if(strncmp(search_data, "artist:", strlen("artist:")) == 0)
            {
                if( p_item->p_input->p_meta )
                {
                    const char *psz_artist = vlc_meta_Get( p_item->p_input->p_meta, vlc_meta_Artist );
                    b_enable = check_data(psz_artist, search_data + strlen("artist:"), use_regex);
                }
            }
            else
            {
                int offset = 0;
                if(strncmp(search_data, "fn:", strlen("fn:")) == 0)
                {
                    offset = strlen("fn:");
                }

                char* psz_filename = input_item_GetFileName(p_item->p_input);
                b_enable = check_data(psz_filename, search_data + offset, use_regex);
                free(psz_filename);
            }

            vlc_mutex_unlock( &p_item->p_input->lock );
        }

        if( b_enable )
            p_item->i_flags &= ~PLAYLIST_DBL_FLAG;
        else
            p_item->i_flags |= PLAYLIST_DBL_FLAG;

        b_match |= b_enable;
   }
   return b_match;
}



/**
 * Launch the recursive search in the playlist
 * @param p_playlist: the playlist
 * @param p_root: the current root item
 * @param psz_string: the string to find
 * @return VLC_SUCCESS
 */
int playlist_LiveSearchUpdate( playlist_t *p_playlist, playlist_item_t *p_root,
                               const char *psz_string, bool b_recursive )
{
    PL_ASSERT_LOCKED;
    pl_priv(p_playlist)->b_reset_currently_playing = true;
    if( *psz_string )
        playlist_LiveSearchUpdateInternal( p_root, psz_string, b_recursive );
    else
        playlist_LiveSearchClean( p_root );
    vlc_cond_signal( &pl_priv(p_playlist)->signal );
    return VLC_SUCCESS;
}

